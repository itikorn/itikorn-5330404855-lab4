/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package kku.coe.webservice;

import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.ServletException;
import javax.servlet.http.*;
import java.io.*;
import org.w3c.dom.*;
import javax.xml.parsers.*;
import java.net.*;
import javax.servlet.annotation.WebServlet;

@WebServlet(name = "FeedSearcherr", urlPatterns = {"/FeedSearcherr"})
public class FeedSearcherr extends HttpServlet {

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        request.setCharacterEncoding("UTF-8");
        PrintWriter out = response.getWriter();
        out.print("<html><body><table	border='1'><tr><th>Title</th><th>Link</th></tr>");
        try {
            //Use JAXP to find parser
            DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();//สร้างobjectของคลาส
            //Turn namespace support
            factory.setNamespaceAware(true);
            DocumentBuilder parser = factory.newDocumentBuilder();//นำ object ที่สร้างมาสร้าง object paser ของคลาส
            String urlLoc = request.getParameter("url");
            String keyword = request.getParameter("keyword");
            URL url = new URL(urlLoc);//รับค่า URL จาก input ของโปรแกรม
            //Read the Entire document to memory
            Document doc = parser.parse(url.openStream()); //อ่านข้อมูลทรัพยากรที่อยู่ใน URL
            NodeList items = doc.getElementsByTagName("item");
            for (int i = 0; i < items.getLength(); i++) {
                Element item = (Element) items.item(i);
                if ((getElemVal(item, "title").toLowerCase()).contains(keyword.toLowerCase())) {
                    out.println("<tr><td>" + getElemVal(item, "title") + "</td>");
                    String link = getElemVal(item, "link");
                    out.println("<td><a	href=	'" + link + "'>" + link + "</a></td><tr>");
                } else {
                    continue;
                }
            }
            out.print("</table></body></html>");
        } catch (Exception e) {
            System.out.print(e);
        } finally {
            out.close();
        }
    }

    protected String getElemVal(Element parent, String lable) {
        Element e = (Element) parent.getElementsByTagName(lable).item(0);
        try {
            Node child = e.getFirstChild();
            if (child instanceof CharacterData) {
                CharacterData cd = (CharacterData) child;
                return cd.getData();
            }
        } catch (Exception ex) {
        }
        return "";
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    @Override
    public String getServletInfo() {
        return "Short	description";
    }
}