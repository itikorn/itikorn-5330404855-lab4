
package kku.coe.webservice;


import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.*;
import javax.servlet.*;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.*;
import org.w3c.dom.*;
import javax.xml.parsers.*;
import javax.xml.transform.*;
import javax.xml.transform.dom.*;
import javax.xml.transform.stream.*;

@WebServlet(name = "FeedWriterForm", urlPatterns = {"/FeedWriterForm"})
public class FeedWriterForm extends HttpServlet {

    Document doc;
    String filePath = "C:/Users/VAIO/Documents/NetBeansProjects/FeedWriterForm/web/feed.xml";
    File file = new File(filePath);

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        request.setCharacterEncoding("UTF-8");
        PrintWriter out = response.getWriter();
         String title1 = request.getParameter("title");
                String link1 = request.getParameter("link");
                String des1 = request.getParameter("description");
        try {
            if (file.exists()) {
                
                DocumentBuilderFactory builderFactory =
                        DocumentBuilderFactory.newInstance();
                DocumentBuilder docBuilder = builderFactory.newDocumentBuilder();
                doc = docBuilder.parse(file);
                FeedWriterForm fw = new FeedWriterForm();
                String feed = fw.updateRssTree(doc, title1, link1, des1);
                out.println(feed);
            } else {
              
                DocumentBuilderFactory builderFactory =
                        DocumentBuilderFactory.newInstance();
                DocumentBuilder docBuilder = builderFactory.newDocumentBuilder();
                doc = docBuilder.newDocument();
                FeedWriterForm fw = new FeedWriterForm();
                String feed = fw.createRssTree(doc, title1, link1, des1);
                out.print(feed);
            }
            System.out.println("Rss");
        } catch (Exception e) {
            System.out.println(e);
            
        }
    }

    public String createRssTree(Document doc, String title1, String link1, String des1) throws Exception {
//root element rss
        Element rss = doc.createElement("rss");
//attribute version = '2.0'in element rss
        rss.setAttribute("version", "2.0");
        doc.appendChild(rss);
//the child element of rss is channel
        Element channel = doc.createElement("channel");
        rss.appendChild(channel);
// Element/rss/channel/title
        Element title = doc.createElement("title");
        channel.appendChild(title);
        Text titleT = doc.createTextNode("Khon Kean University Rss Feed");
        title.appendChild(titleT);
//element /rss/channel/description
        Element desc = doc.createElement("description");
        channel.appendChild(desc);
        Text descT = doc.createTextNode("Khon Kea University Information News Rss Feed");
        desc.appendChild(descT);
//element/rss/channel/link
        Element link = doc.createElement("link");
        channel.appendChild(link);
        Text linkT = doc.createTextNode("http://www.kku.ac.th");
        link.appendChild(linkT);
//element/rss/channel/lang
        Element lang = doc.createElement("lang");
        channel.appendChild(lang);
        Text langT = doc.createTextNode("en-th");
        lang.appendChild(langT);
//element/rss/channel/item
        Element item = doc.createElement("item");
        channel.appendChild(item);
//element/rss/channel/title
        Element iTitle = doc.createElement("title");
        item.appendChild(iTitle);
        Text iTitleT = doc.createTextNode(title1);
        iTitle.appendChild(iTitleT);
        Element iDesc = doc.createElement("description");
        item.appendChild(iDesc);
        Text iDescT = doc.createTextNode(des1);
        iDesc.appendChild(iDescT);
        Element iLink = doc.createElement("link");
        item.appendChild(iLink);
        Text iLinkT = doc.createTextNode(link1);
        iLink.appendChild(iLinkT);
        Element pubDate = doc.createElement("pubDate");
        item.appendChild(pubDate);
        Text pubDateT = doc.createTextNode((new java.util.Date()).toString());
        pubDate.appendChild(pubDateT);
//Transformerfactory instance is used to create Transformer objects.
        TransformerFactory factory = TransformerFactory.newInstance();
        Transformer transformer = factory.newTransformer();
        transformer.setOutputProperty(OutputKeys.INDENT, "yes");
//create string from xml tree
        StringWriter sw = new StringWriter();
        StreamResult result = new StreamResult(sw);
        DOMSource source = new DOMSource(doc);
        transformer.transform(source, result);
        String xmlString = sw.toString();
//set to an appropriate file name in the web project
        BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(file)));
        bw.write(xmlString);
        bw.flush();
        bw.close();
        return xmlString;
    }

    public String updateRssTree(Document doc, String title1, String link1, String des1) throws Exception {
        Element channel = (Element) doc.getElementsByTagName("channel").item(0);
        Element item = doc.createElement("item");
        channel.appendChild(item);
        Element iTitle = doc.createElement("title");
        item.appendChild(iTitle);
        Text iTitleT = doc.createTextNode(title1);
        iTitle.appendChild(iTitleT);
        Element iDesc = doc.createElement("description");
        item.appendChild(iDesc);
        Text iDescT = doc.createTextNode(des1);
        iDesc.appendChild(iDescT);
        Element iLink = doc.createElement("link");
        item.appendChild(iLink);
        Text iLinkT = doc.createTextNode(link1);
        iLink.appendChild(iLinkT);
        Element pubDate = doc.createElement("pubDate");
        item.appendChild(pubDate);
        Text pubDateT = doc.createTextNode((new java.util.Date()).toString());
        pubDate.appendChild(pubDateT);
//TransformerFactory instance is used to create transformer objects.
        TransformerFactory factory = TransformerFactory.newInstance();
        Transformer transformer = factory.newTransformer();
        transformer.setOutputProperty(OutputKeys.INDENT, "yes");
//create string from xml tree
        StringWriter sw = new StringWriter();
        StreamResult result = new StreamResult(sw);
        DOMSource source = new DOMSource(doc);
        transformer.transform(source, result);
        String xmlString = sw.toString();
//set to an appropriate file name in the web project
        BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(file)));
        bw.write(xmlString);
        bw.flush();
        return xmlString;
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    @Override
    public String getServletInfo() {
        return "Short description";
    }
// </editor-fold>
}