/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package kku.coe.webservice;


import com.sun.xml.internal.txw2.output.IndentingXMLStreamWriter;
import java.io.IOException;
import java.io.PrintWriter;


import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.*;
import javax.xml.stream.*;
import javax.xml.stream.events.Attribute;
import javax.xml.stream.events.Characters;
import javax.xml.stream.events.EndElement;
import javax.xml.stream.events.StartElement;
import javax.xml.stream.events.XMLEvent;


@WebServlet(name =
"FeedWriterForm2", urlPatterns = {"/FeedWriterForm2"})
public class FeedWriterForm2 extends HttpServlet {

   String filename =           
"C:/Users/VAIO/Documents/NetBeansProjects/FeedWriterForm2/web/feed2.xml";
        File file = new File(filename);

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text / html;charset = UTF-8");
        PrintWriter out = response.getWriter();
        try {
            String title1 = request.getParameter("title");
            String link1 = request.getParameter("link");
            String des1 = request.getParameter("description");
            //check is xml file exist
            if (file.isFile()) {
                new FeedWriterForm2().updateRssDoc(title1, link1, des1);
            } else {
                new FeedWriterForm2().createRssDoc(title1, link1, des1);
            }
            out.println("<b><a href='http://localhost:8080/FeedWriterForm2/feed2.xml'>RSS Feed</a> was created suxxessfully</b>");
        } catch (Exception e) {
            System.err.println(e);
        }
    }

    private void createRssDoc(String title1, String des1, String link1) throws Exception {
         XMLOutputFactory xmlOutputFactory = XMLOutputFactory.newInstance();
        XMLStreamWriter xtw = xmlOutputFactory.createXMLStreamWriter(new OutputStreamWriter(new FileOutputStream(file),"utf-8"));
        xtw = new IndentingXMLStreamWriter(xtw);

             xtw.writeStartDocument();
        xtw.writeStartElement("rss");
        xtw.writeAttribute("version", "2.0");
        xtw.writeStartElement("channel");
        xtw.writeStartElement("title");
        xtw.writeCharacters("Khon Kean  University RSS  Feed");
        xtw.writeEndElement();//end     title
        xtw.writeStartElement("description");
        xtw.writeCharacters("Khon Kean  University Information  News RSS        Feed");
        xtw.writeEndElement();//end     description
        xtw.writeStartElement("link");
        xtw.writeCharacters("http://www.kku.ac.th");
        xtw.writeEndElement();//end     link
        xtw.writeStartElement("lang");
        xtw.writeCharacters("en~th");
        xtw.writeEndElement();//end     lang
        //element/rss/channel/item
        xtw.writeStartElement("item");
        //element/rss/channel/item/title
        xtw.writeStartElement("title");
        xtw.writeCharacters(title1);
        xtw.writeEndElement();
        xtw.writeStartElement("description");
        xtw.writeCharacters(des1);
        xtw.writeEndElement();
        xtw.writeStartElement("link");
        xtw.writeCharacters(link1);
        xtw.writeEndElement();
        xtw.writeStartElement("pubDate");
        xtw.writeCharacters((new java.util.Date()).toString());
        xtw.writeEndElement();
        xtw.writeEndElement();//end     element item
        xtw.writeEndElement();//end     element channel
        xtw.writeEndElement();//end     element rss
        xtw.writeEndDocument();
        xtw.flush(); //ส่งข้อมูล XML ทั้งหมดในไฟล์
        xtw.close(); //ปิด XMLStreamWriter
        
    }
  private void updateRssDoc(String title1, String link1, String des1) throws Exception {
        XMLInputFactory xif = XMLInputFactory.newInstance();
        XMLEventReader xtr = xif.createXMLEventReader(new InputStreamReader(new FileInputStream(file)));
        XMLOutputFactory xof = XMLOutputFactory.newInstance();
        XMLStreamWriter xtw = xof.createXMLStreamWriter(new OutputStreamWriter(new FileOutputStream(file),"utf-8"));
        String eName;
        while (xtr.hasNext()) {
            XMLEvent event = xtr.nextEvent();
            if (event.isStartDocument()) {
                xtw.writeStartDocument();
            }
            if (event.isStartElement()) {
                StartElement element = (StartElement) event;
                eName = element.getName().getLocalPart();
                xtw.writeStartElement(eName);
            }
            if (event.isAttribute()) {
                Attribute attribute = (Attribute) event;
                String n = attribute.getName().getLocalPart();
                String v = attribute.getValue();
                xtw.writeAttribute(n, v);
            }
            if (event.isEndElement()) {
                EndElement element = (EndElement) event;
                eName = element.getName().getLocalPart();
                if (eName.equals("channel")) {
                    xtw.writeStartElement("item");

                    xtw.writeStartElement("title");
                    xtw.writeCharacters(title1);
                    xtw.writeEndElement();


                    xtw.writeStartElement("description");
                    xtw.writeCharacters(des1);
                    xtw.writeEndElement();


                    xtw.writeStartElement("link");
                    xtw.writeCharacters(link1);
                    xtw.writeEndElement();


                    xtw.writeStartElement("pubDate");
                    xtw.writeCharacters((new java.util.Date()).toString());
                    xtw.writeEndElement();


                    xtw.writeEndElement();

                    xtw.writeEndDocument();
                    xtw.flush();
                    xtw.close();
                    
                } else {
                    xtw.writeEndElement();

                }
            }
            if (event.isCharacters()) {
                Characters characters = (Characters) event;
                xtw.writeCharacters(characters.getData().toString());
            }
        }
      
        

    }
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }
}