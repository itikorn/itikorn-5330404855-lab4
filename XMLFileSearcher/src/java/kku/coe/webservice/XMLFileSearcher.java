/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package kku.coe.webservice;


import com.sun.xml.fastinfoset.algorithm.BuiltInEncodingAlgorithm;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.Scanner;
import javassist.compiler.ast.Keyword;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import org.w3c.dom.CharacterData;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;


public class XMLFileSearcher {

    public static void main(String argv[]) throws FileNotFoundException, UnsupportedEncodingException, ParserConfigurationException, SAXException, IOException {
        
        String keyword;
        String keywordFile = "keyword.txt";
        File xmlFile = new File("quotes.xml");
       
        
        Scanner scanner = new Scanner(new FileInputStream(keywordFile));
       
        try {
            while (scanner.hasNext()) {
                
                keyword = scanner.next();
                
                DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
                DocumentBuilder dBuilder = factory.newDocumentBuilder();
                Document doc = dBuilder.parse(xmlFile);
                doc.getDocumentElement().normalize();
                   
                NodeList quotes1 = doc.getElementsByTagName("quote");

                for (int i = 0; i < quotes1.getLength(); i++) {
                        
                    Element item = (Element) quotes1.item(i);
                    //System.out.println(getElemVal(item,"by"));
                    if (getElemVal(item, "by").toLowerCase().contains(keyword.toLowerCase())) {
                        System.out.println(getElemVal(item, "word") + " by " + getElemVal(item, "by"));
                        
                    }else{
                        continue;
                    }
                }
            }
            
        } 
       //catch (Exception e) {
          //  System.out.println(e);
       //}
        finally {
           scanner.close();
        }
    }

    protected static String getElemVal(Element parent, String lable) {
        Element e = (Element) parent.getElementsByTagName(lable).item(0);
        try {
            Node child = e.getFirstChild();
            if (child instanceof CharacterData) {
                CharacterData cd = (CharacterData) child;
                return cd.getData();
            }
        } catch (Exception ex) {
        }
        return "";
    }
}