
package kku.coe.webservice;


import java.io.*;
import javax.xml.parsers.*;
import javax.xml.transform.*;
import javax.xml.transform.dom.*;
import javax.xml.transform.stream.*;
import org.w3c.dom.*;

public class XMLFileWriter {
    
    //Document doc;
    //String filePath = "C:/Users/VAIO/Documents/NetBeansProjects/XMLFileWriter/web/quotes.xml";
    //File file = new File(filePath);
    
  public static void main(String[] args) throws Exception {
    BufferedReader bf = new BufferedReader(new InputStreamReader(System.in));
//    System.out.print("Enter number to add elements in your XML file: ");
//    String str = bf.readLine();
    int no=2;
//    System.out.print("Enter root: ");
    String root = "qoutes";
    DocumentBuilderFactory documentBuilderFactory =DocumentBuilderFactory.newInstance();

        DocumentBuilder documentBuilder =documentBuilderFactory.newDocumentBuilder();
        Document document = documentBuilder.newDocument();
        Element rootElement = document.createElement(root);
        document.appendChild(rootElement);
//    for (int i = 1; i <= no; i++)
//      System.out.print("Enter the element: ");
//      String element = bf.readLine();
        
System.out.println("Create file quotes.xml finish");
        Element quote = document.createElement("quote");
        rootElement.appendChild(quote);
        
        Element word = document.createElement("word"); 
        quote.appendChild(word);
        Text data = document.createTextNode("Time is more value than money. You can get more money, but you cannot get more time");
        word.appendChild(data);
        
        
        Element by = document.createElement("by"); 
        quote.appendChild(by);
        Text data1 = document.createTextNode("Jim Rohn");
        by.appendChild(data1);
        
        Element quote1 = document.createElement("quote");
        rootElement.appendChild(quote1);
        
        Element word1 = document.createElement("word"); 
        quote1.appendChild(word1);
        Text data2 = document.createTextNode("เมื่อทำอะไรสำเร็จ แม้เป้นก้าวเล็กๆ ก็ควรรู้จักให้รางวันลตัวเองบ้าง");
         word1.appendChild(data2);
    
        
        Element by1 = document.createElement("by");
        quote1.appendChild(by1);
        Text data3 = document.createTextNode("ว. วชิรเมธี"); 
        by1.appendChild(data3);
       
        
TransformerFactory tf = TransformerFactory.newInstance();
            Transformer trans = tf.newTransformer();
            trans.setOutputProperty(OutputKeys.INDENT, "yes");
            DOMSource source = new DOMSource(document);
            StreamResult result = new StreamResult(new File("web/qoutes.xml"));

            trans.transform(source, result);

          
  }
}