/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package kku.coe.webservice;

import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.net.URL;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.stream.XMLEventReader;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.events.Characters;
import javax.xml.stream.events.EndElement;
import javax.xml.stream.events.StartElement;
import javax.xml.stream.events.XMLEvent;

@WebServlet(name = "FeedSearcherr2", urlPatterns = {"/FeedSearcherr2"})
public class FeedSearcherr2 extends HttpServlet {

    XMLEventReader reader;

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        request.setCharacterEncoding("UTF-8");
        PrintWriter out = response.getWriter();
        String input = request.getParameter("url");
        String keyword = request.getParameter("keyword");
        String titleData = null; //มีค่าแต่แสดงออกมาเป็นไม่มีค่
        String linkData = null;
        boolean titleFound = false; //มีตัวแปรตรวจสอบว่าเป็น element ที่ต้องการหรือไม่
        boolean linkFound = false;
        boolean itemFound = false;
        String link = null;
        String eName;
        try {
            URL u = new URL(input); //รับค่า URL จาก input ของโปรแกรม
            InputStream in = u.openStream(); //สร้าง InputStream เพื่ออ่านข้อมูลจากเว็บของ URL ที่ระบุ
            XMLInputFactory factory = XMLInputFactory.newInstance(); //สร้าง XMLInputFactory เพื่อจะสร้าง reader
            reader = factory.createXMLEventReader(in); //render จะอ่านข้อมูลจาก URL ที่ระบุ
            out.print(
                    "<html><body><table border	='1'><tr > <th>Title</th > <th>Link</th ></tr> ");
            while (reader.hasNext()) { //hasNext อ่านต่อไปเรื่อยๆ
                XMLEvent event = reader.nextEvent();
                if (event.isStartElement()) {
                    StartElement element = (StartElement) event;
                    eName = element.getName().getLocalPart();
                    if (eName.equals("item")) {
                        itemFound = true;
                    }
                    if (itemFound && eName.equals("link")) {
                        linkFound = true;
                        //  out.print("<td>"); //แสดงตารางเพิ่ม
                    }
                    if (itemFound && eName.equals("title")) {
                        titleFound = true;
                        // out.print("<tr><td>"); //แสดงตารางเพิ่ม
                    }

                }
                if (event.isEndElement()) {
                    EndElement element = (EndElement) event;
                    eName = element.getName().getLocalPart();
                    if (eName.equals("item")) {
                        itemFound = false;
                    }
                    if (itemFound && eName.equals("link")) {
                        // out.print("</td></tr>"); //ไม่มีผลอะไร
                        linkFound = false;
                    }
                    if (itemFound && eName.equals("title")) {
                        // out.print("</td>"); //ไม่มีผลอะไร
                        titleFound = false;
                    }

                }
                if (event.isCharacters()) {
                    Characters characters = (Characters) event;
                    //search title with keyword.
                    
                    if (titleFound) {
                        titleData = characters.getData();
                    }



                    if (linkFound) {
                        linkData = characters.getData();
                            
                        if (titleData.toLowerCase().contains(keyword.toLowerCase())) {
                            out.print("<tr><td>" + titleData + "</td>");
                            out.print("<td><a href='" + linkData + "'>" + linkData + "</a></td></tr>");

                        }
                    }
                    





                }
            }

            //	end	while
            reader.close();
            out.print("</table></body></html>");
        } catch (Exception ex) {
            ex.printStackTrace(System.err);
        } finally {
            out.close();
        }
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    @Override
    public String getServletInfo() {
        return "Short	description";
    }
}