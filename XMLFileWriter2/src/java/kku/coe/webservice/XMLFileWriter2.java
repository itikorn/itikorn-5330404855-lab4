/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package kku.coe.webservice;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.stream.XMLOutputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamWriter;

/**
 *
 * @author VAIO
 */
public class XMLFileWriter2{
    
 
    public static void main(String[] args) throws XMLStreamException, FileNotFoundException{
        
        // Create file directory save
                String filename ="C:/Users/VAIO/Documents/NetBeansProjects/XMLFileWriter2/web/quotes.xml";
	File file = new File(filename);
        
         // Create an output factory
      XMLOutputFactory xmlof = XMLOutputFactory.newInstance();
      
      // Set namespace prefix defaulting for all created writers
     // xmlof.setProperty("javax.xml.stream.isPrefixDefaulting",Boolean.TRUE);
      
      // Create an XML stream writer
      XMLStreamWriter xmlw =
         xmlof.createXMLStreamWriter(new OutputStreamWriter(
                new FileOutputStream(file)));
System.out.println("Create file quotes.xml finish");
      // Write XML prologue
      xmlw.writeStartDocument();
      // Write a processing instruction
      xmlw.writeProcessingInstruction(
         "xml-stylesheet href='feed2.xsl' type='text/xsl'");
      xmlw.writeStartElement("quotes");
        xmlw.writeStartElement("quote");
            xmlw.writeStartElement("word");
            xmlw.writeCharacters("Time is more value than money. You can get more money, but you cannot get more time");
            xmlw.writeEndElement();
            xmlw.writeStartElement("by");
            xmlw.writeCharacters("Jim Rohn");
            xmlw.writeEndElement();
        xmlw.writeEndElement();
        xmlw.writeStartElement("quote");
            xmlw.writeStartElement("word");
            xmlw.writeCharacters("เมื่อทำอะไรสำเร็จ แม้เป้นก้าวเล็กๆ ก็ควรรู้จักให้รางวันลตัวเองบ้าง");
            xmlw.writeEndElement();
            xmlw.writeStartElement("by");
            xmlw.writeCharacters("ว. วชิระเมธี");
            xmlw.writeEndElement();
       xmlw.writeEndElement();
xmlw.writeEndElement();
xmlw.writeEndDocument();
xmlw.close();

    }
}


