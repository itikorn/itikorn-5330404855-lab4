/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package kku.coe.webservice;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.net.URL;
import java.util.Scanner;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.stream.XMLEventReader;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLOutputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamReader;
import javax.xml.stream.XMLStreamWriter;
import javax.xml.stream.events.Characters;
import javax.xml.stream.events.EndElement;
import javax.xml.stream.events.StartElement;
import javax.xml.stream.events.XMLEvent;
import org.w3c.dom.CharacterData;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;
/**
 *
 * @author VAIO
 */



public class XMLFileSearcher2 {

    XMLEventReader reader;

    public static void main(String argv[]) throws FileNotFoundException, XMLStreamException {     
        String keywordFile = "keyword.txt";
        File quoteFile = new File("qoutes.xml");
        
        boolean qouteFound = false;
        boolean wordFound = false;
        boolean byFound = false;
        String keyword = null;
        String word = null;
        String by = null;
        String eName = null;
        Scanner scanner = new Scanner(new FileInputStream(keywordFile),"UTF-8");
      
    

        try {
            while (scanner.hasNext()) {
                keyword = scanner.next();
                XMLInputFactory factory = XMLInputFactory.newInstance();
                XMLEventReader reader = factory.createXMLEventReader(new InputStreamReader(new FileInputStream(quoteFile)));

                while (reader.hasNext()) {
                    XMLEvent event = reader.nextEvent();
                    if (event.isStartElement()) {
                        StartElement element = (StartElement) event;
                        eName = element.getName().getLocalPart();
                        if (eName.equals("quote")) {
                            qouteFound = true;
                        }
                        if (qouteFound && eName.equals("word")) {
                            wordFound = true;
                        }
                        if (qouteFound && eName.equals("by")) {
                            byFound = true;
                        }
                    }

                    if (event.isEndElement()) {
                        EndElement element = (EndElement) event;
                        eName = element.getName().getLocalPart();
                        if (eName.equals("quote")) {
                            qouteFound = false;
                        }
                        if (qouteFound && eName.equals("word")) {
                            wordFound = false;
                        }
                        if (qouteFound && eName.equals("by")) {
                            byFound = false;
                        }
                    }

                    if (event.isCharacters()) {
                        Characters characters = (Characters) event;
                        
                        if(wordFound) {
                            word = characters.getData();
                        }
                        if(byFound) {
                            by = characters.getData();
                            
                            if((by.toLowerCase()).contains(keyword.toLowerCase())) {
                                System.out.println(word + " by " + by);
                            }
                        }
                        
                    }
                }
            }

        } 
        catch (Exception ex) {
            
       }
        finally {
            scanner.close();
        }
    }
}